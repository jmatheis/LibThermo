function [x, y, psi_v, F, success, iter_o] = solveTPN( nc, z, temp, press, K_init )
% SOLVETPN  Equilibrium calculation at specified temperature T, pressure p 
%           and overall composition z using the successive subsitution
%           method and optionally a Newton method. 
%
% Inputs: 
%    nc         - Number of components
%    z(nc)      - Overall composition
%    temp       - Temperature
%    press      - Pressure
%    K_init(nc) - Initial guess K-factors (optional)
%
% Outputs:
%    x(nc)      - Liquid phase composition
%    y(nc)      - Vapor phase composition
%    psi_v      - Vapor fraction on a molar basis [mol vapor / mol mixture]
%    F(nc)      - Objective function; Residuum
%    success    - Success?
%    iter_o     - Number outer loop iterations (SSI + Newton)
%
% Author: 
%    Jan Matheis, Technical University Munich, jan.matheis@gmail.com
%
% Please cite this work as: 
%    J. Matheis and S. Hickel (2017), Multi-component vapor-liquid equilibrium 
%    model for LES of high-pressure fuel injection and application to ECN Spray A
%    Int. J. Multiph. Flow, doi: 10.1016/j.ijmultiphaseflow.2017.11.001
%
% Literature:
%    REF1: Michelsen (1982) The isothermal flash problem. Part I. Stability
%    REF2: Hoteit & Firoozabadi (2006) Simple phase stability?testing algorithm in the reduction method
%    REF3: Li & Firoozabadi (2012) General Strategy for Stability Testing and Phase-Split Calculation in Two and Three Phases
%    REF4: Michelsen & Mollerup (2007) Thermodynamic Models: Fundamentals & Computational Aspects
%    REF5: Saha & Carroll (1997) The isoenergetic-isochoric flash
%    REF6: Abbas Firoozabadi - Thermodynamics of Hydrocarbon Reservoirs

global R_univ 
global u_EOS w_EOS
global slst

success     = false;
print_info  = false;

% NOTE: The Newton method of the TPn flash was not used in the 
% simulations documented in my dissertation 
% J. Matheis, "Numerical Simulation of Fuel Injection and 
%              Turbulent Mixing Under High-Pressure Conditions" and
% J. Matheis and S. Hickel (2017) "Multi-component vapor-liquid equilibrium 
% model for LES of high-pressure fuel injection and application to ECN
% Spray A", Int. J. Multiph. Flow, doi: 10.1016/j.ijmultiphaseflow.2017.11.001
% The SSI approach (translated into FORTRAN) was sufficient. 
% Because I planned to include also Newton methods into the real gas module
% in the LES code INCA (www.inca-cfd.org) I tested them here in MATLAB.  
use_Newton = true; 
full_jacobian = true; % Use analytical Jacobian with composition dependent fugacity coefficients
                      % If false: Use approximated Jacobian; this should yield
                      % the same convergence rate as the SSI method, cf. REF4 p.255

% You can also use fsolve to solve the TPn flash; 
% This is particulary helpful if you need to check the analystical Jacobian
% use_fsolve = true will print the numerical and analytical Jacobian matrix
use_fsolve  = false;


tol_o       = 1.e-07; % convergence criterium outer loop (SSI) 
tol_i       = 1.e-07; % convergence criterium inner loop (Rachford-Rice)
tol_SSI     = 1.e-04; % convergence criterium for first SSI steps 
tol_trivial = 1.E-06; % identify the trivial solution x=y=z

itmax_o = 1000; % itmax outer loop
itmax_i = 100; % itmax inner loop

z = z / sum(z(1:nc)); % Feed composition normalization

if nargin > 4
    K = K_init;
else
    % Initial estimates of the K-factors via the Wilson equation
    for i=1:nc
        K(i) = slst(i).pc / press ...
             * exp( 5.37 * ( 1 + slst(i).omega ) * ( 1 - slst(i).Tc / temp ) );
    end    
end

iter_o = 0; % Counter outer loop

x = zeros(1,nc);  % Liquid phase composition
y = zeros(1,nc);  % Vapor phase composition
F = zeros(1,nc);  % Objective function 
v = zeros(1,nc);  % v_i = psi_v * y_i (Remember: l_i = ( 1 - psi_v ) * x_i and z_i = l_i + v_i)
J = zeros(nc,nc); % Jacobian matrix

lnphi_v = zeros(1,nc); % Logarithm of fugacity coefficient
lnphi_l = zeros(1,nc); % 

dlnphidn_l = zeros(nc,nc); % Derivative of logarithm of fug. coeff. with respect
dlnphidn_v = zeros(nc,nc); % to mole number n_i in the corresponding phase

kronDel = @(j, k) j==k;


while ( iter_o <= itmax_o ) % Outer loop iteration 

    % Solve Rachford-Rice; Pseudocode in REF4, p.252, Chapter 10. 
    if sum(K.*z) >= 1 && sum(z./K) >= 1 

        eps_i  = 1.0;
        iter_i = 1;
        
        psi_v_min = 0.;
        psi_v_max = 1.;
        
        for i=1:nc
            if K(i) > 1
                psi_v_min = max(0.,( K(i) * z(i) - 1. ) / ( K(i) - 1. ) );
            else
                psi_v_max = min(1.,( 1. - z(i) ) / ( 1. - K(i) ) );
            end
        end
           
        psi_v = 0.5 * (psi_v_min + psi_v_max);
      
        while (eps_i >= tol_i && iter_i <= itmax_i)

            RR  = 0.;
            dRR = 0.;
            for i=1:nc
                % Rachford-Rice Equation
                RR  = z(i) * ( K(i) - 1. ) / ( 1. + psi_v * ( K(i) - 1. ) ) + RR;
                % Derivative of Rachford-Rice Equation
                dRR = z(i) * ( K(i) - 1. )^2 / ( 1. + psi_v * ( K(i) - 1. ) )^2 + dRR;
            end
            dRR = -dRR;

            if RR > 0.
                psi_v_min = psi_v; % Root is located in the 'upper' half of the interval.
            else
                psi_v_max = psi_v; % We should change to psi_l as iteration variable.
                                   % Root is located in 'lower' half of the interval.
                                   % See REF4 p.253
            end
            
            % Newton-Raphson
            dpsi_v = - (RR / dRR);
            psi_vn = psi_v + dpsi_v;
            
            eps_i  = abs( ( psi_vn - psi_v ) / psi_v );

            if psi_vn > psi_v_min && psi_vn < psi_v_max
                psi_v  = psi_vn;
            else
                psi_v = 0.5 * (psi_v_min + psi_v_max);
            end   
            
            iter_i = iter_i + 1;
            
        end
        
        for i=1:nc
            x(i) = z(i) / ( 1.0 + psi_v * ( K(i) - 1.0 ) );
            y(i) = K(i) * x(i); % same as : y(i) = z(i) * K(i) / ( 1 + ( K(i) - 1 ) * psi_v );
        end    

    % See Michelsen 1982 PART II S. 25
    elseif sum(z./K) <= 1
        psi_v      = 1.; 
        for i=1:nc
            x(i) = z(i)/K(i);
            y(i) = z(i);
        end 
        x=x/sum(x(1:nc)); % Normalization 
    elseif sum(K.*z) <= 1
        psi_v      = 0.; 
        for i=1:nc
            x(i) = z(i);
            y(i) = K(i) * z(i); 
        end   
        y=y/sum(y(1:nc)); % Normalization    
    end 

    % Get mixed EOS params for liq. and vap. phase 
    [aalpha_l, ~, ~, b_l, aalpha_jk_l, ~] = mix_GCEOS( nc, x, temp );
    [aalpha_v, ~, ~, b_v, aalpha_jk_v, ~] = mix_GCEOS( nc, y, temp );
    
    % Get liq. and vap. volume / compressibility factor
    % Remark: Passing 'Vap' to vol_GCEOS may exclude the creation of LLEs
    [vol_v, ~] = vol_GCEOS( u_EOS, w_EOS, aalpha_v, b_v, temp, press, 'Vap' );
    Z_v = vol_v(1) * press / ( R_univ * temp );

    [vol_l, ~]= vol_GCEOS( u_EOS, w_EOS, aalpha_l, b_l, temp, press, 'Liq' );
    Z_l = vol_l(1) * press / ( R_univ * temp );
    
    % Calculate fugacity coeffcients
	for i=1:nc; 
       lnphi_v(i) = lnphi_GCEOS( u_EOS, w_EOS, slst(i).b, b_v, aalpha_jk_v(i), aalpha_v, Z_v, temp, press );
       lnphi_l(i) = lnphi_GCEOS( u_EOS, w_EOS, slst(i).b, b_l, aalpha_jk_l(i), aalpha_l, Z_l, temp, press );
    end
    
    % Objective function, see e.g. Ref: Saha & Carroll (1997) Eq. 21
	for i=1:nc; 
        F(i) = lnphi_l(i) - lnphi_v(i) - log(K(i));
    end
    
    eps  = sqrt(abs(F*F')); % Resdiuum
    
     % Converged with SSI method
    if eps <= tol_o
        success = true;
        break
        
    % Proceed with Newton Method
    elseif eps <= tol_SSI && use_Newton == true
        break 
    
    % Update K-factors from fugacity coeffcients
    else  
        for i=1:nc; 
            K(i) = exp(lnphi_l(i)-lnphi_v(i));
        end
    end

    iter_o = iter_o + 1;
    
end


% This turned out to be important! No Newton method if we did not solve the Rachford-Rice equation
if abs(psi_v - 1) < tol_trivial || abs(psi_v) < tol_trivial 
    use_Newton = false;  
end

% No need for Newton if SSI converged
if success
    use_Newton = false;
end 

if print_info
    fprintf('eps_SSI = %12.6e \n',eps);   
    fprintf('ite_SSI = %4i \n',iter_o); 
end 

%% Use MATLAB's fsolve and compare numerical and analytical Jacobian.
if use_fsolve
    % Options for fsolve
    options = optimoptions(@fsolve,'Jacobian','off'... % fsolve approximates Jacobian using FD
                                  ,'TolFun',1.0000e-8 ...
                                  ,'Tolx', 1.0e-8 );
    
    % z_i = psi_v * y_i + ( 1 - psi_v ) * x_i
    % with v_i =       psi_v   * y_i
    %      l_i = ( 1 - psi_v ) * x_i
    %      z_i = v_i + l_i
    for i=1:nc;
        v_init(i) = y(i) * psi_v;
    end

    % Scale problem manually (this should help fsolve)
    v_ref  = v_init;
    v_init = v_init ./ v_ref;

    % Create function handle for fsolve
    fTPn = @(v) objectiveTPN( v, z, temp, press, v_ref );
    
    % Solve TPn flash with fsolve
    [v0,F,exitflag,~,jac_fsolve] = fsolve(fTPn,v_init,options); 
    % Error handling
    if not(any(exitflag==[1 2 3 4]))
        error('fsolve did not converge! exitflag = %u | residuum = %12.6e \n',exitflag,F)
    else
        success = true;
    end   
    % Print numerical Jacobian of fTPn at solution v0
    fprintf( 'Numerical Jacobian:  %8.4f %8.4f \n',jac_fsolve(1,1),jac_fsolve(1,2));     
    fprintf( '                     %8.4f %8.4f \n\n',jac_fsolve(2,1),jac_fsolve(2,2));     


    % Calculate analytical Jacobian via fTPn
    [F, jac_analytic] = fTPn(v0);
    fprintf( 'Analytical Jacobian: %8.4f %8.4f \n',jac_analytic(1,1),jac_analytic(1,2));     
    fprintf( '                     %8.4f %8.4f \n',jac_analytic(2,1),jac_analytic(2,2)); 

    % Switch back to molar flows
    v0 = v0 .* v_ref;

    % Get phase compositions
    psi_v = sum(v0);
    for i=1:nc;
        y(i) = v0(i) / psi_v;
        x(i) = ( z(i) - v0(i) ) / ( 1 - psi_v );
    end  
    return   
end 


%% Newton procedure for solving the flash equations. 
if use_Newton == true
    
    % Switch to molar flows, a.k.a. vapor phase component amounts v_i
    % v_i is used as independet set of variables. Liquid amounts are 
    % calculated later from from the overall material balance l_i = z_i - v_i 
    for i=1:nc;
        v(i) = y(i) * psi_v;
    end

    while iter_o < itmax_o

        % get mixed EOS params for liq. and vap. phase 
        [aalpha_l, ~, ~, b_l, aalpha_jk_l, ~        ] = mix_GCEOS( nc, x, temp ); % aalpha_jk(i,j) = a(i,j) * alpha(i,j) 
        [aalpha_v, ~, ~, b_v, aalpha_jk_v, aalpha_jk] = mix_GCEOS( nc, y, temp ); % -> Does not depend on x and y 

        [vol_v, ~] = vol_GCEOS( u_EOS, w_EOS, aalpha_v, b_v, temp, press, 'Vap' );
        Z_v   = vol_v(1) * press / ( R_univ * temp );


        [vol_l, ~]= vol_GCEOS( u_EOS, w_EOS, aalpha_l, b_l, temp, press, 'Liq' );
        Z_l   = vol_l(1) * press / ( R_univ * temp );

        % Calculate fugacity coeffcients phi and 
        % derivatives of the fugacity with respect to n_i
        for i=1:nc
            lnphi_v(i) = lnphi_GCEOS( u_EOS, w_EOS, slst(i).b, b_v, aalpha_jk_v(i), aalpha_v, Z_v, temp, press );
            lnphi_l(i) = lnphi_GCEOS( u_EOS, w_EOS, slst(i).b, b_l, aalpha_jk_l(i), aalpha_l, Z_l, temp, press );
            for j=1:nc
                dlnphidn_v(i,j) = dlnphidn_GCEOS( u_EOS, w_EOS, slst(i).b, slst(j).b, b_v, aalpha_jk_v(i), aalpha_jk_v(j), aalpha_jk(i,j), aalpha_v, Z_v, temp, press );  
                dlnphidn_l(i,j) = dlnphidn_GCEOS( u_EOS, w_EOS, slst(i).b, slst(j).b, b_l, aalpha_jk_l(i), aalpha_jk_l(j), aalpha_jk(i,j), aalpha_l, Z_l, temp, press );  
            end
        end

        if full_jacobian
            % Assembly the exact analytical Jacobian matrix
            for i=1:nc; 
                for j=1:nc

                    work_v = 0.;
                    work_l = 0.;
                    for k=1:nc
                        work_v = work_v + dlnphidn_v(i,k) * ( kronDel(j,k) - y(k) );
                        work_l = work_l + dlnphidn_l(i,k) * ( x(k) - kronDel(j,k) );
                    end 

                    J(i,j) = 1. / ( psi_v * ( 1. - psi_v ) ) ...
                           * ( z(i) / ( x(i) * y(i) ) * kronDel(i,j) - 1. ...
                           + ( 1. - psi_v ) * work_v - psi_v * work_l );

                end
            end
        else
            % Assemply approximated Jacobian (REF4 255 Eq. 14)
            for i=1:nc;
                for j=1:nc
                    J(i,j) = 1. / ( psi_v * ( 1. - psi_v ) ) ...
                             * ( z(i) / ( x(i) * y(i) ) * kronDel(i,j) - 1. );
                end
            end
        end 
        
        % Objective function (f_i^L = f_i^V) expressed in terms
        % of fugacity coefficients, vapor phase component amounts v_i and
        % vapor fraction psi_v
        for i=1:nc; 
            F(i) = lnphi_v(i) - lnphi_l(i) + log( ( v(i) * ( 1. - psi_v ) ) ...
                                                / ( psi_v * ( z(i) - v(i) ) ) );
        end

        eps  = sqrt(abs(F*F')); % Resdiuum

        if eps <= tol_o
            success = true;
            break
        else

            % See Chapter 10, pp 255,256 in REF4
            % If we use the approximate Jacobian we can use the following 
            % formula to update v (rather than v = v - dv with dv = J\F')
            % Convergence rate should be the same as for the SSI approach
            % when using the approximate Jacobian 

            % for i=1:nc; 
            %     s(i) = x(i) * y(i) / z(i);
            % end

            % for i=1:nc; 
            %     dv(i) = psi_v * ( 1 - psi_v ) * s(i) * ( s * F' / ( sum(s) - 1. ) - F(i) );
            %     v(i)  = v(i) + dv(i);
            % end 

            % Take a full Newton step 
            dv = J\F';
            v = v - dv';

            % Update x, y, and psi_v after Newton step
            psi_v = sum(v);
            for i=1:nc;
                y(i) = v(i) / psi_v;
                x(i) = ( z(i) - v(i) ) / ( 1 - psi_v );
            end    
        end % eps <= tol_o 
        iter_o = iter_o + 1;   
    end
end 

if print_info 
    fprintf('eps_New = %12.6e \n',eps);   
    fprintf('ite_New = %4i \n',iter_o);
end


if success

    % REF6: "For mixtures, when gas and liquid phases are at equilibrium, Z_l
    % might be smaller or larger than Z_v. At equilibrium, the mass density
    % of the liquid phase is higher than the mass density of the gas phase,
    % rho_l > rho_v (mass basis). With rho = p*M/(ZRT) it follows that 
    % M_l/Z_l > M_v/Z_v"

    M_l = 0.;
    M_v = 0.;
    for i=1:nc
        M_l = M_l + x(i) * slst(i).M; % mixture molar mass in liquid phase
        M_v = M_v + y(i) * slst(i).M; % mixture molar mass in liquid phase
    end
    
    % Switch here.
    if M_l/Z_l < M_v/Z_v
        tmp = Z_v; Z_v = Z_l; Z_l = tmp; 
        tmp = x; x   = y; y   = tmp;
        psi_v = 1 - psi_v;
    end   
   
end

