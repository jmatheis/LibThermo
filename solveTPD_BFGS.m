function [stable, TPDmin, K_init_min] = solveTPD_BFGS( nc, z, temp, press )
% SOLVETPD_BFGS   Perform TPD analysis using the BFGS method
%
% Inputs: 
%    nc           - Number of components
%    w_trial(nc)  - Trial phase composition 
%    z_test(nc)   - Composition z that is tested  
%    temp, press  - Temperature and pressure
%
% Outputs:
%    stable      - Stable? (true or false)
%    TPDmin      - Mimimum value of non-dimensional tpd function 
%    K_init_min  - K-factor which led to convergence towards TPDmin  
%
% Authors: 
%    Jan Matheis, Technical University Munich, jan.matheis@gmail.com
%
% Please cite this work as: 
%    J. Matheis and S. Hickel (2017), Multi-component vapor-liquid equilibrium 
%    model for LES of high-pressure fuel injection and application to ECN Spray A
%    Int. J. Multiph. Flow, doi: 10.1016/j.ijmultiphaseflow.2017.11.001

% Literature:
%    REF1: Michelsen (1982) The isothermal flash problem. Part I. Stability
%    REF2: Hoteit & Firoozabadi (2006) Simple phase stability testing algorithm in the reduction method
%    REF3: Li & Firoozabadi (2012) General Strategy for Stability Testing and Phase-Split Calculation in Two and Three Phases
%    REF4: Michelsen & Mollerup Thermodynamic Models / Fundamentals and Computational Aspects
%    REF5: Dan Vladimir Nichita: Phase stability testing near the stability test limit

global R_univ
global u_EOS w_EOS
global slst


print_info        = false;
be_negative       = -1.E-8; % Define what is negative numerically
be_pure_substance = 1. - 1.E-8; % Define what we mean by pure substance
be_very_large     = 1.E10; % Define what is a huge number

% No TPD analysis for "pure" substances
if any(z >= be_pure_substance) 
    TPDmin     = 10.;
    stable     = true;
    K_init_min = 0.;
    return 
end

ntrial    = 4; % Number of trial compositions we will use to find the 
               % (global) minium of the TPD function. Note that this
               % approach does not guarantee to locate the global minumum
               % of the TPD.
               
tol       = 1.E-9; % Residuum for convergence as in REF2
iter_max  = 1000;
y_sp      = zeros(ntrial,nc); % Stationary points of TPD

% Get mixed EOS params of assumed single phase z
[aalpha_z, ~, ~, b_z, aalpha_jk_z, ~] = mix_GCEOS( nc, z, temp );

% Get compressibility factor of assumed single phase z
[vol_z, phase_z] = vol_GCEOS( u_EOS, w_EOS, aalpha_z, b_z, temp, press, 'Gibbs' );
Z_z = vol_z(1) * press / ( R_univ * temp );

% Calculate fugacity coeffcients of assumed single phase z 
for i=1:nc; 
   lnphi_z(i) = lnphi_GCEOS( u_EOS, w_EOS, slst(i).b, b_z, aalpha_jk_z(i), aalpha_z, Z_z, temp, press );
   d(i) = log( z(i) ) + lnphi_z(i); % Eqn. 3 in REF2
end

% REF4, S. 270 : "The procedure suggested here for stability analysis is 
% very conservative in the sense that it attempts to minimise the risk of 
% missing a two-phase solution. In most cases, converging stability analysis 
% twice, with initial estimates from both eqns. (49) and (50) represents 
% wasted effort, as at least one of these estimates is very likely to 
% converge to the trivial solution. If a phase identification for the 
% feed can be made with confidence (as 'liquid-like' or as 'vapour-like'), 
% a one-sided stability analysis is adequate. Such a phase identification 
% is, however, difficult to perform in the critical region, and if emphasis 
% is on safety the two-sided approach is the only 'guarantee' against 
% misinterpreting the phase distribution."
% Depending on the result of phase_z we order Y_init(1:2,:) and Y_init(3:4,:)  
% if phase_z == Liq -> trial phase composition should be Vap
% if phase_z == Vap -> trial phase composition should be Liq
% See also Eq. A2. in REF2
% We use the Wilson equation to generate trial phase compositions
for i=1:nc
    K(i) = slst(i).pc/press*exp(5.37*(1+slst(i).omega)*(1-slst(i).Tc/temp));
end 

if strcmp(phase_z,'Liq')
    Y_init(1,:) = z .* K;          % If trial phase is vap (-> tested phase z is liquid)
    Y_init(2,:) = z .* K.^(1./3.); % If trial phase is vap (-> tested phase z is liquid)  
    Y_init(3,:) = z ./ K;          % If trial phase is liq (-> tested phase z is vapor)
    Y_init(4,:) = z ./ K.^(1./3.); % If trial phase is liq (-> tested phase z is vapor)
else 
    Y_init(1,:) = z ./ K;          % If trial phase is liq (-> tested phase z is vapor)
    Y_init(2,:) = z ./ K.^(1./3.); % If trial phase is liq (-> tested phase z is vapor)
    Y_init(3,:) = z .* K;          % If trial phase is vap (-> tested phase z is liquid)
    Y_init(4,:) = z .* K.^(1./3.); % If trial phase is vap (-> tested phase z is liquid)   
end 
% REMARK: We could add here some more initial values - K^new test from REF3 

% Loop over all trial compositions
% REMARK: In principle we must try all trial compositions if we do not 
%         want to miss the global minimum of TPD. This is especially
%         important in cases in which the solution of a TPn flash is a 
%         liq.-liq. equilibrium. However, if we know in advance that
%         there is no liq.-liq. equilibrium, we can sort K_init in a way
%         as described above. And abort the search if we detect a negative 
%         TPD value.
for k=1:ntrial % Loop over all trial phase compositions
    
    for i=1:nc
        Y(i) = Y_init(k,i); % Initialize Yi (Eq. A2. in REF2)
    end 
    
    y_trial = Y/sum(Y); % Normalize mole numbers y_i = Y_i / sum(Y)
    [aalpha_y, ~, ~, b_y, aalpha_jk_y,~] = mix_GCEOS( nc, y_trial, temp );
    [vol_y, phase_y] = vol_GCEOS( u_EOS, w_EOS, aalpha_y, b_y, temp, press, 'Gibbs' );
    Z_y = vol_y(1) * press / ( R_univ * temp );
    
    for i=1:nc
        alpha_old(i) = 2. * sqrt(Y(i));
        lnphi_y      = lnphi_GCEOS( u_EOS, w_EOS, slst(i).b, b_y, aalpha_jk_y(i), aalpha_y, Z_y, temp, press );    
        F_old(i)     = alpha_old(i) / 2. * (lnphi_y + log(Y(i)) - d(i));
        alpha_new(i) = 2. * exp( 0.5 * ( lnphi_y - d(i) ) );
    end

    % The algorithm here is documented in REF2
    for iter = 1:iter_max

        for i=1:nc
            Y(i) = 0.25 * alpha_new(i)^2; % Initialize Yi from Eq. A2. in REF2       
        end 
        
        y_trial = Y/sum(Y);

        [aalpha_y, ~, ~, b_y, aalpha_jk_y] = mix_GCEOS( nc, y_trial, temp );
        [vol_y, phase_y] = vol_GCEOS( u_EOS, w_EOS, aalpha_y, b_y, temp, press, 'Gibbs' );
        Z_y = vol_y(1) * press / ( R_univ * temp ) ;

                 
        TPD_star(k) = 1.; 
        beta = 0.;
        
        for i=1:nc
            lnphi_y  = lnphi_GCEOS( u_EOS, w_EOS, slst(i).b, b_y, aalpha_jk_y(i), aalpha_y, Z_y, temp, press );    
            F_new(i) = alpha_new(i) / 2. * ( lnphi_y + log(Y(i)) - d(i) );
            TPD_star(k) = TPD_star(k) + Y(i) * ( lnphi_y + log(Y(i)) - d(i) - 1 );
            beta = beta + ( Y(i) - z(i) ) * ( lnphi_y + log(Y(i)) - d(i) ); % Eq. 16 in REF2 with Eq. 13 p 235 in REF4 
        end
        
        g         = F_new - F_old;
        F_old     = F_new;
        s         = alpha_new - alpha_old;
        alpha_old = alpha_new;

        F_times_s   = ( F_old * s' );
        F_times_g   = ( F_old * g' );
        s_times_g   = ( s * g' );
        g_times_g   = ( g * g' );

        dalpha      = ( g *   F_times_s ...
                         - s * ( F_times_s - F_times_g ...
                               + g_times_g * F_times_s / s_times_g  ) ...
                         ) / s_times_g ...
                       - F_old;
   
        
        alpha_new = alpha_old + dalpha;
                                            
        eps = sqrt(dalpha*dalpha');
        
        r = 2 * TPD_star(k) / beta; % Eq. 16 in REF2 
       
        if eps <= tol

            y_sp(k,:) = Y/sum(Y); % Stationary points of TPD and TPD_star function 
            
            TPD(k)    = -log( sum(Y) ); % App. A Algorithm Step 6 in REF2
                                        % If I understood thing right, its
                                        % OK to check the sign of TPD_star
                                        % which is computaional less
                                        % intensive at this point 
            
            TPD_star(k)  = 1. - sum(Y); % Only true at the staionary points! 
                                        % otherwise: 
                                        % TPD_star = 1 + SUM( Y(i) * ( lnphi_y + log(Y(i)) - d(i) - 1 ) )
                                 
            mask_stationary(k) = true;
            
            % Leave solveTPD_BFGS here.
            % Take care: We might miss the true TPD minimum if we leave
            % the TPD test here. We might have found a minimum, but not the
            % global minimum. 
            if TPD_star(k) < be_negative  && true
                   
                stable = false; 
                TPDmin = TPD_star(k);
                
                % Back to K-Factors
                if strcmp(phase_z,'Liq') 
                    switch k
                        case 1
                           K_init_min = Y_init(k,:) ./ z(:)'; 
                        case 2
                           K_init_min = (Y_init(k,:) ./ z(:)').^3;
                        case 3
                           K_init_min =  z(:)' ./ Y_init(k,:); 
                        case 4
                           K_init_min = (z(:)' ./ Y_init(k,:)).^3;
                    end
                else 
                    switch k
                        case 1
                           K_init_min =  z(:)' ./ Y_init(k,:); 
                        case 2
                           K_init_min = (z(:)' ./ Y_init(k,:)).^3;
                        case 3
                           K_init_min = Y_init(k,:) ./ z(:)'; 
                        case 4
                           K_init_min = (Y_init(k,:) ./ z(:)').^3;
                    end
                end  

                if print_info
                    formatSpec = '  TPD: Phase(z) = %s | z(1) = %8.4f | z(2) = %8.4f \n';
                    fprintf(formatSpec,phase_z,z(1),z(2));  
                    formatSpec = '  TPD: Phase(y) = %s | y(1) = %8.4f | y(2) = %8.4f | k = %1i | TPD_star = %+12.6e | nit = %4i | Resid = %+12.6e \n';
                    fprintf(formatSpec,phase_y,y_sp(k,1),y_sp(k,2),k,TPD_star(k),iter,eps); 
                end
                
                return % leave function here
            
            end % Return immediately when negative TPD detected
                   
            break % move on to the next trial phase composition because eps <= tol
            
        end % eps < tol
        
        if abs(r-1) < 0.1 ... % REF2 recommends 0.2, but I observed situations in which a non-trivial solution was rejected.
                || any(alpha_new > be_very_large) % Divergence can occur, see REF5. This here is just a work around to stop calculating with NaNs or Infs.
                          
            TPD(k) = 10.;
            TPD_star(k) = 10.; 
            y_sp(k,:) = -1;
            mask_stationary(k) = false;
            break % move on to the next trial phase composition
                  % because abs(r-1) < 0.1
        end 

    end % Maximum iterations
    
    if print_info
        formatSpec = '  TPD: Phase(z) = %s | z(1) = %8.4f | z(2) = %8.4f \n';
        fprintf(formatSpec,phase_z,z(1),z(2));  
        formatSpec = '  TPD: Phase(y) = %s | y(1) = %8.4f | y(2) = %8.4f | k = %1i | TPD_star = %+12.6e | nit = %4i | Resid = %+12.6e \n';
        fprintf(formatSpec,phase_y,y_sp(k,1),y_sp(k,2),k,TPD_star(k),iter,eps); 
    end 
    
    
end % Trials

TPDmin = min(TPD_star);
if TPDmin < be_negative % Eqn. 4 in REF2
    % Return the K-factor which led to lowest TPD value as initial guess
    % for the TPn flash calculation. 
    % (Recommended by REF3, Phase-Split Calculation, p. 1098)
    
    stable = false; 
    
    [TPDmin, iTPDmin] = min(TPD_star);

    % Back to K-Factors
    if strcmp(phase_z,'Liq') 
        switch iTPDmin
            case 1
               K_init_min = Y_init(iTPDmin,:) ./ z(:)'; 
            case 2
               K_init_min = (Y_init(iTPDmin,:) ./ z(:)').^3;
            case 3
               K_init_min =  z(:)' ./ Y_init(iTPDmin,:); 
            case 4
               K_init_min = (z(:)' ./ Y_init(iTPDmin,:)).^3;
        end
    else 
        switch iTPDmin
            case 1
               K_init_min =  z(:)' ./ Y_init(iTPDmin,:); 
            case 2
               K_init_min = (z(:)' ./ Y_init(iTPDmin,:)).^3;
            case 3
               K_init_min = Y_init(iTPDmin,:) ./ z(:)'; 
            case 4
               K_init_min = (Y_init(iTPDmin,:) ./ z(:)').^3;
        end
    end 
        
else
           
    stable = true;
    K_init_min = 0.;
    
end 
