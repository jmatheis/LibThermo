function a_i = a_PR( R_univ, Tc_i, pc_i, ~ )
% A_PR  Calculate a parameter in Peng-Robinson EOS
%
% Inputs: 
%    R_univ     - Universal gas constant
%    Tc_i       - Critical temperature
%    pc_i       - Critical temperature
%
% Outputs:
%    a          - "a" parameter in Peng-Robinson EOS (without "alpha(T)")
%
% Author: Jan Matheis, Technical University Munich, jan.matheis@gmail.com
%
a_i = 0.457236 * R_univ * R_univ * Tc_i * Tc_i / pc_i;

end

