function b_i = b_PR( R_univ, Tc_i, pc_i, ~ )
% B_PR  Calculate "b" parameter in Peng-Robinson EOS (co-volume)
% 
% Inputs: 
%    R_univ     - Universal gas constant
%    Tc_i       - Critical temperature
%    pc_i       - Critical temperature
%
% Outputs:
%    b          - "b" parameter in Peng-Robinson EOS
%
% Author: Jan Matheis, Technical University Munich, jan.matheis@gmail.com

b_i = 0.077796 * R_univ * Tc_i / pc_i;

end

