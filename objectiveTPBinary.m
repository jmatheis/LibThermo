function F = objectiveTPBinary( xy, temp, press )
% OBJECTIVETPBINARY   Objective function which is solved to calculate dew- and bubble-point lines for binary mixtures.
%
% Inputs: 
%    xy(nc)       - xy(1) mole of component 1 in the liq. phase
%                 - xy(2) mole of component 1 in the vap. phase
%    temp, press  - Temperature and pressure
%
% Outputs:
%    F            - Objective function to be minimized 
%
% Authors: 
%    Jan Matheis, Technical University Munich, jan.matheis@gmail.com
% 
% Literature:
%   REF1: Saha & Carroll (1997) The iso-energetic-isochoric flash
%   REF2: Michelsen & Mollerup Thermodynamic Models Fundamentals and Computational Aspects, Chapter 12, Sec.5 (p.310)

global R_univ 
global u_EOS w_EOS
global slst
global nc 

% Here: Binary mixture assumption
x_n(1) =     xy(1);  % Mole fraction of component 1 in the liq. phase
x_n(2) = 1 - xy(1);  % Mole fraction of component 2 in the liq. phase
y_n(1) =     xy(2);  % Mole fraction of component 1 in the vap. phase
y_n(2) = 1 - xy(2);  % Mole fraction of component 2 in the vap. phase

[aalpha_v, ~, ~, b_v, aalpha_jk_v] = mix_GCEOS( nc, y_n, temp);
[aalpha_l, ~, ~, b_l, aalpha_jk_l] = mix_GCEOS( nc, x_n, temp);

% Get liq. and vap. volume / compressibility factor
[vol_v, ~] = vol_GCEOS( u_EOS, w_EOS, aalpha_v, b_v, temp, press, 'Gibbs' );
Z_v   = vol_v(1) * press / ( R_univ * temp );

[vol_l, ~] = vol_GCEOS( u_EOS, w_EOS, aalpha_l, b_l, temp, press, 'Gibbs' );
Z_l   = vol_l(1) * press / ( R_univ * temp );

% Calculate logarithm of fugacity coefficient for both phases
for i=1:nc; 
   lnphi_v(i) = lnphi_GCEOS( u_EOS, w_EOS, slst(i).b, b_v, aalpha_jk_v(i), aalpha_v, Z_v, temp, press );
   lnphi_l(i) = lnphi_GCEOS( u_EOS, w_EOS, slst(i).b, b_l, aalpha_jk_l(i), aalpha_l, Z_l, temp, press );
end

for i=1:nc; 
    K(i) = y_n(i) / x_n(i); % Calculate K-factors
end

% Objective function, see, e.g. Eq. 21 in REF1
for i=1:nc; 
    F(i) = lnphi_l(i) - lnphi_v(i) - log(K(i)); 
end
    
end