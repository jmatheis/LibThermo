 function dlnphidn = dlnphidn_GCEOS( u_EOS, w_EOS ...
                                   , db_i, db_k, b ...
                                   , daalpha_i, daalpha_k, aalpha_ik, aalpha ...
                                   , Z, temp, press )
% DLNPHIDN_GCEOS   Calculate the partial derivative of the fugacity coefficient
%                  of component i with respect to mole number k 
%
% Inputs: 
%    u_EOS, w_EOS - Parameter for generalized cubic EOS
%    db_i, db_k   - co-volume of component i, k 
%    b            - ...
%
% Outputs:
%    dlnphidn     - d(log(phi_i))dn_k 
%
% Author: 
%    Jan Matheis, Technical University Munich, jan.matheis@gmail.com
%
% Literature:
%    

global R_univ
global EOS 
global delta_1 delta_2 % (Not so nice here. The derivative should be expressed in terms of u_EOS and w_EOS which are passed as input arguments)


% = delta_1 - delta_2 = sqrt( u_EOS^2 - 4. * w_EOS )

if strcmp(EOS,'PR') || strcmp(EOS,'SRK')
    % OK: Move on.
else
    error(['Make sure the derivative works for your EOS.' ...
           'For RK-PR you need to take the composition dependency' ...
           'of u & w / delta_1 & delta_2 into account.'])
end

p_inv_RT  = 1. / ( R_univ * temp );
p_inv_RT2 = p_inv_RT^2 * press;
p_inv_RT  = p_inv_RT * press;

B_i   = ( db_i  ) * p_inv_RT;
B     = ( b  ) * p_inv_RT;
B_k   = ( db_k  ) * p_inv_RT;

A_i   = ( daalpha_i  ) * p_inv_RT2;
A     = ( aalpha  ) * p_inv_RT2;
A_k   = ( 2 * daalpha_k  ) * p_inv_RT2;
A_ik  = ( aalpha_ik ) * p_inv_RT2;

C2 = ( B * ( u_EOS - 1. ) - 1. );
C1 = ( A + B * ( w_EOS * B - u_EOS * B - u_EOS ) );
C0 = - B * ( w_EOS * B^2 + w_EOS * B + A );

C2_k = ( u_EOS - 1. ) * B_k;
C1_k = A_k - u_EOS * B_k - 2. * ( u_EOS - w_EOS ) * B * B_k;
C0_k =  B_k * C0 / B - B * (2 * w_EOS * B * B_k + w_EOS * B_k + A_k); 

Z_k = - 1. / ( 3. * Z^2 + 2. * C2 * Z + C1 ) * ( Z^2 * C2_k + Z * C1_k + C0_k );

dlnphidn = B_i / B * Z_k ...
         - B_i / B^2 * ( Z - 1. ) * B_k ...
         - 1. / ( Z - B ) * ( Z_k - B_k ) ...
         - A / ( ( delta_1 - delta_2 ) * B ) *  ( 2. * A_i / A - B_i / B ) * ( 1. / ( Z + delta_1 * B ) * ( Z_k + delta_1 * B_k ) - 1. / ( Z + delta_2 * B ) * ( Z_k + delta_2 * B_k ) ) ...
         - log( ( Z + delta_1 * B ) / ( Z + delta_2 * B ) ) * ( 2. / ( ( delta_1 - delta_2 ) * B ) * A_ik - 2. * A_i / ( ( delta_1 - delta_2 ) * A * B ) * A_k + A * B_i / ( ( delta_1 - delta_2 ) * B^3 ) * B_k  ) ...
         - ( 2. * A_i / A - B_i / B ) * log( ( Z + delta_1 * B ) / ( Z + delta_2 * B ) ) * ( 1. / ( ( delta_1 - delta_2 ) * B ) * A_k - A / ( ( delta_1 - delta_2 ) * B^2 ) * B_k  ) ;

