function [vol, u , h, cv, cp, sound, dvdx_Tp, phase] = prop_GCEOS( u_EOS, w_EOS    ...
                                                     , aalpha, daalphadT, ddaalphadT, aalpha_jk ...
                                                     , b, coeffs, temp, press, root )
% PROP_GCEOS   Objective function (not the solution) for equilibrium  
%              calculation at specified enthalpy, pressure and overall  
%              composition. fsolve can do the job.
%
% Inputs: 
%    u_EOS, w_EOS  - Parameter for generalized cubic EOS
%    aalpha,
%    daalpha,
%    ddaalpha      - a*alpha(T) in cubic EOS and partial derivatives
%    aalpha_jk(nc) - Vector with k entries of summe_{j=1}^{N_c}(z_j*a_jk*alpha_jk) 
%    b             - Co-volume in cubic EOS
%    coeffs        - Coefficients NASA polynomials
%    temp          - Temprature
%    press         - Pressure
%    root          - root mode, see vol_GCEOS
%
% Outputs:
%    vol            - Molar volume
%    u              - Internal energy
%    h              - Enthalpy
%    cv, cp         - Heat capacities
%    dvdx_Tp(nc)    - Vector of partial molar volumes d(nV)dn_i|_{T,p,i \neq j} 
%    phase          - Guess what this phase might be, see vol_GCEOS
%
%
% Author: 
%    Jan Matheis, Technical University Munich, jan.matheis@gmail.com


global slst
global R_univ
global nc        


if nargin < 11
   error('Wrong number of arguements'); 
end

[tmp, phase] = vol_GCEOS( u_EOS, w_EOS, aalpha, b, temp, press, root );
vol = tmp(1);

[hNASA, cpNASA] = NASA( temp, coeffs );

[~ , dh, ~, ~, dcv] = dep_GCEOS( u_EOS, w_EOS, aalpha ...
                               , daalphadT, ddaalphadT ...
                               , b, temp, vol );

h = hNASA + dh;
u = h - press * vol;

nenner  = ( vol^2 + u_EOS * b  * vol + w_EOS * b^2 );

dpdT_v  = R_univ / ( vol - b ) - daalphadT / nenner;
dpdv_T  = ( ( 2. * vol + u_EOS * b )  * aalpha ) / nenner^2 ...
        - R_univ * temp / ( vol - b )^2;
dvdp_T  = 1. / dpdv_T;

cvNASA     = cpNASA - R_univ; 
cv         = cvNASA + dcv;
cp         = cv - temp * dpdT_v^2 / dpdv_T;
sound      = vol * sqrt( - cp / cv * dpdv_T );


for i=1:nc

    b_l = slst(i).b;

    dvdx_Tp(i) = - dvdp_T ...
       * ( ( R_univ * temp / ( vol - b ) ) ...
       + ( b_l * R_univ * temp /  ( vol - b )^2 ) ...
       - ( 2. * aalpha_jk(i) / nenner ) ...
       + ( aalpha * ( u_EOS * vol ...
                    + 2. * w_EOS * b ) * b_l ...
         / nenner^2 ) ); % Partial molar volume 


end 